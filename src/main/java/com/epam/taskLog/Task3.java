package com.epam.taskLog;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task3 {
    private static Logger logger = LogManager.getLogger(Task3.class);

    public Task3() {
    }

    public void run() {
        logger.trace("This is trace logger");
        logger.debug("This is debug logger");
        logger.info("This is info logger");
        logger.warn("This is warn logger");
        logger.error("This is error logger");
        logger.fatal("This is fatal logger");
    }
}
