package com.epam.taskLog;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class TaskMenu {
    Map<String, String> menu;
    Map<String, Runnable> methodMenu;
    Scanner input;

    public TaskMenu() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Display in the console and save to a file");
        menu.put("2", "2 - Info - save to the file, Debug - console");
        menu.put("3", "3 - Not overwritten file; Daily overwritten file; Overwritten after 1 Mb; Overwritten " +
                "daily and overwrite to a new file");
        menu.put("4", "4 - Configure log, which is the strongest in the lower WARNING, is stored in the file");
        menu.put("5", "5 - Send sms om phone");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::task1);
        methodMenu.put("2", this::task2);
        methodMenu.put("3", this::task3);
        methodMenu.put("4", this::task4);
        methodMenu.put("5", this::task5);


    }

    public void task1() {
        Task1 t1 = new Task1();
        t1.run();
    }

    public void task2() {
        Task2 t2 = new Task2();
        t2.run();
    }

    public void task3() {
        Task3 t3 = new Task3();
        t3.run();
    }

    public void task4() {
        Task4 t4 = new Task4();
        t4.run();
    }

    public void task5() {
        Task5 t5 = new Task5();
        t5.run();
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void run() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
