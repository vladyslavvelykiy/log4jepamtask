package com.epam.taskLog;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;


public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACacf265ecf902cea77b5d1c46abc46a04";
    public static final String AUTH_TOKEN = "eb7ceb9323fabc2e6d1d5952fdac0fcb";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message msg = Message
                .creator(new PhoneNumber("+380675877628"),
                        new PhoneNumber("+18599273601"), str).create();
    }
}
